<!--
SPDX-FileCopyrightText: 2021 codedust

SPDX-License-Identifier: EUPL-1.2
-->

# FIM-Datenfelder-Stats

Statistiken der FIM-Datenfeldschema aus dem [FIT-Connect-Schemarepository](https://schema.fitko.de/fim/).
Korrektheit der Daten und Statistiken ohne Gewähr.

## License
Licensed under the [EUPL](./LICENSE).
