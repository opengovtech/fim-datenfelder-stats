// SPDX-FileCopyrightText: 2021 codedust
// SPDX-License-Identifier: EUPL-1.2

// get DOM elements
const elSumTotal = document.getElementById('sum-total');
const elSumPartial = document.getElementById('sum-current-version');
const elNumEditors = document.getElementById('sum-editors');

// retrieve schema repo index
fetch("https://schema.fitko.de/fim/").then(response => response.text()).then(function(response) {
  // parse html
  var parser = new DOMParser();
  var dom = parser.parseFromString(response, 'text/html');

  // select <a> elements
  const aElements = Array.from(dom.querySelectorAll('ul li a')).slice(1);

  // extract schema ids and versions from schema urls. Resulting type: [[schemaId, version]]
  const schemas = aElements.map(a => new URL(a.href).pathname.replace(location.pathname, '').replace('.schema.json', '').split('_'));

  // display total count
  elSumTotal.innerText = schemas.length;

  // display total count excluding old versions
  let sumSchemasExcludingOldVersions = new Set(schemas.map(s => s[0])).size;
  elSumPartial.innerText = sumSchemasExcludingOldVersions;

  // display number of editors (Redaktionen)
  elNumEditors.innerText = new Set(schemas.map(s => s[0].slice(1, 3))).size;

  // display stats for federation and states
  let sumSchemas = 0;
  for (var i = 0; i <= 17; i++) {
    let stateIdpadded = ("0" + i).slice(-2);

    // retrieve number of schemas (expluding previous versions)
    let numSchmas = Array.from(
      new Set(schemas.map(s => s[0])) // exclude previous schema versions
    ).filter(s => s.slice(1, 3) == stateIdpadded).length; // filter by state

    let el = document.getElementById('sum-' + stateIdpadded).innerText = numSchmas || '-';
    sumSchemas += numSchmas;
  }

  // check of schemas are missing
  if (sumSchemas != sumSchemasExcludingOldVersions) {
    console.warn("Schemas missing!", sumSchemas, "vs", sumSchemasExcludingOldVersions);
  }

}).catch(function (e) {
  console.exception(e);
  document.querySelector('p').innerText = "Fehler beim Aufruf des Schema-Repository. :/";
});
